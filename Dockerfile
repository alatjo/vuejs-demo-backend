FROM java:8
MAINTAINER Joni
CMD ["mvn", "clean package"]
COPY src/main/resources/application-dev.properties /usr/src/restApi/application.properties
COPY target/vue-app-backend-0.0.1-SNAPSHOT.jar /usr/src/restApi/vue-app-backend-0.0.1-SNAPSHOT.jar
WORKDIR /usr/src/restApi
RUN apt-get update
CMD ["java", "-jar", "vue-app-backend-0.0.1-SNAPSHOT.jar"]