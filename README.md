# README #

### What is this repository for? ###

* This is full backend for https://bitbucket.org/alatjo/vuejs-demo
* This include setting up Cassandra DB (with mock data) and Rest Api
* RestApi can be called via http://localhost:8080/bills

### How do I get set up? ###

* Set up Cassandra DB
```
#!shell
docker-compose up -d cassandra
```

* Execute mock data to cassandra DB
```
#!shell
docker exec -it vueappbackend_cassandra_1 cqlsh -f /mnt/data/backend.cql
```

* Set up Rest Api
```
#!shell
docker-compose up api -d
```




