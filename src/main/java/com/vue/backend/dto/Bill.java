package com.vue.backend.dto;

import java.util.UUID;

import org.springframework.data.cassandra.mapping.Column;
import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

import com.datastax.driver.core.LocalDate;

@Table(value="bills")
public class Bill {
	
	public Bill(UUID id, String type, String description, int amount, LocalDate rawDate) {
		this.id = id;
		this.type = type;
		this.description = description;
		this.amount = amount;
		this.dateSimple = getDate(rawDate);
		this.rawDate = rawDate;
	}
	
	//TODO refactor to return correctly formatted date object
    private String getDate(LocalDate rawDate) {
		return new StringBuilder()
				.append(rawDate.getMonth())
				.append("/")
				.append(rawDate.getDay())
				.append("/")
				.append(rawDate.getYear()).toString();
	}

	@PrimaryKey
    private UUID id;
    
    @Column("type")
    private String type;
    
    @Column("description")
    private String description;
    
    @Column("amount")
    private int amount;
    
    @Column("date")
    private LocalDate rawDate;
    
    private String dateSimple;
    
    public LocalDate getRawDate() {
		return rawDate;
	}

	public void setRawDate(LocalDate rawDate) {
		this.rawDate = rawDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getDateSimple() {
		return dateSimple;
	}

	public void setDateSimple(String date) {
		this.dateSimple = date;
	}	
}