package com.vue.backend.repository;
import org.springframework.data.repository.CrudRepository;

import com.vue.backend.dto.Bill;

import java.util.UUID;


public interface BillRepository extends CrudRepository<Bill, UUID> {}