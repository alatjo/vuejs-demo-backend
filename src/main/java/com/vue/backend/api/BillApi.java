package com.vue.backend.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vue.backend.dto.Bill;
import com.vue.backend.repository.BillRepository;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class BillApi {
	
	@Autowired
    private BillRepository billRepository;
	
	@RequestMapping(value="/bills",method=RequestMethod.GET)
    public @ResponseBody List<Bill> getBills() {
    	return (List<Bill>) billRepository.findAll();
    }
}
